var express = require('express'),
app = express(),
port = process.env.PORT || 3000;

var path = require('path');

//Se indica que los componentes los tome de la raíz
app.use(express.static(__dirname + '/build/default'));

app.listen(port);

console.log("polymerdesdenode en puerto: " + port);

app.get('/', function(req, res) {
//Se indica que los componentes los tome de la raíz  
  res.sendFile(path.join(__dirname, 'index.html', {root: '.'}));
})
